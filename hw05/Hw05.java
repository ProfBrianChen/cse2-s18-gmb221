//Georgia Burlingame
// HW 06 
// 03.06.2018
// Purpose of program is to obtain inputs from the user and check to see if they are of a certain variable type, if not it will as the user again 

import java.util.Scanner;

public class Hw05 {
  public static void main(String[] args){
    
    Scanner courseScanner = new Scanner(System.in); // creates new scanner 
    System.out.print("Enter the course number: "); // asks user for input
    boolean crn = courseScanner.hasNextInt(); //accepts as boolean 
   
    
    // if not an integer will ask again for one 
    while (!courseScanner.hasNextInt()) {
      courseScanner.next(); // throws away the previous answer 
      System.out.print("Error, you need to enter an integer type. Please enter course number again: "); //asks again for an int
      
      // if it is an int it accepts answer 
      if(courseScanner.hasNextInt() == true){
        courseScanner.nextInt(); // accepts value as an int 
        break; //breaks out of this while loop and goes to the next one because the input provided was an int 
         
       }
    }  
    
    Scanner departmentScanner = new Scanner(System.in); // creates new scanner 
    System.out.print("Enter the department name: "); // asks user for input 
    boolean department = departmentScanner.hasNext(); // accepts input as a boolean so while loop can be made 
   
   
    // when input is not a string or if it is an int or double it will run through while loop and ask user again 
    while (!departmentScanner.hasNextLine() || departmentScanner.hasNextInt() || departmentScanner.hasNextDouble()){
      departmentScanner.next(); // discards previous answer 
      System.out.print("Error. Please enter a string type. Please enter Department name again: "); // asks user again 
      
      // if the answer new input is a string and not an int or double it will accept the value as the department 
      if (departmentScanner.hasNextLine() == true && departmentScanner.hasNextInt() == false && departmentScanner.hasNextDouble() == false){
        departmentScanner.nextLine(); // accepts input as a string 
        break; // since the if statement was true, it does not need to keep asking user so it breaks out of this while statement 
        
      }
      
    }
    
    Scanner timesMeetScanner = new Scanner(System.in); // creates new scanner 
    System.out.print("Enter the amount of times the class meets per week: "); // asks user for input 
    boolean classesMeet = timesMeetScanner.hasNextInt(); // accepts input as a boolean so while loop will run 
    
    // when input is not an integer it will go throug while loop
    while (!timesMeetScanner.hasNextInt()){
      timesMeetScanner.next(); // discards previous answer 
      System.out.print("Error, please enter an integer type. Please enter the amount of times per week you meet again: "); // asks the user for an input again 
      
      // if the new input is an integer it will accept that value 
      if(timesMeetScanner.hasNextInt() == true){
        timesMeetScanner.nextInt(); // accepts the value that is an integer 
        break; // breaks out of the loop if and while statement 
        
      }
      
      
    }
    
    Scanner classTimeScanner = new Scanner(System.in); // creates new scanner 
    System.out.print("Enter the time the class starts like XX.XX:  "); // asks user for input 
    boolean classStarts = classTimeScanner.hasNextDouble(); // accepts input as a boolean so while loop will run 
    
    // when input is not a double, or if it is an integer or if it is a float it will go through the while loop
    while (!classTimeScanner.hasNextDouble() || classTimeScanner.hasNextInt() || classTimeScanner.hasNextFloat()){
      classTimeScanner.next(); // discards previous answer 
      System.out.print("Error, please enter an double type. Please enter when the class begins: "); // ask the user for new input
      
      // if new input is a double this if statement will run, if not it will go through this while loop again 
      if(classTimeScanner.hasNextDouble() == true){
        classTimeScanner.nextDouble(); // accepts the value as a double 
        break; //breaks out of the loop of if and while statement 
        
      }
    }
    
    Scanner nameInstScanner = new Scanner(System.in); // creates new scanner 
    System.out.print("Enter the name of the instructor: "); // asks user for input 
    boolean teacher = nameInstScanner.hasNext(); // accepts input as a boolean so it will run through while loop
    
    
    // when the input provide is NOT a string or it IS a int or IS a double it will run through while statement 
    while(!nameInstScanner.hasNextLine() || nameInstScanner.hasNextInt() || nameInstScanner.hasNextDouble()){
      nameInstScanner.next(); // discards previous input 
      System.out.print("Error, please enter a string type. Enter the name of Instructor again: "); // asks user for another input 
      
      // if new input is a string and if it is not an int and not a double it will run through if statement, otherwise will go through while loop again 
      if (nameInstScanner.hasNextLine() == true && nameInstScanner.hasNextInt() == false && nameInstScanner.hasNextDouble() == false){
        nameInstScanner.nextLine(); // accepts the value as a string 
        break; // breaks out of while loop 
       
      }
      
    }
    
    Scanner studentNumberScanner = new Scanner(System.in); // creates new scanner 
    System.out.print("Enter the number of students: "); // asks user for input 
    boolean students = studentNumberScanner.hasNextInt(); // accepts input as a boolean so it can go through while statement 
   
    // when the input provide is not an int it will run through while statement 
    while(!studentNumberScanner.hasNextInt()){
      studentNumberScanner.next(); // discards previous input 
      System.out.print("Error. Please enter an Integer Type. Please enter the number of student again: "); // ask user for a new input 
      
      // if new input is an integer it will run through if statement, otherwise it will go back through while statement 
      if(studentNumberScanner.hasNextInt() == true){
        studentNumberScanner.nextInt(); // accepts value as an integer 
        break; // breaks out of while loop 
  
        
      }
    }
    
  
}
}