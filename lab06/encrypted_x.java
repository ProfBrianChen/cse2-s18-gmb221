// Georgia Burlingame 
// CSE 2
// Lab 6

import java.util.Scanner;

public class encrypted_x{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // new scanner 
    
    System.out.print("Please enter an integer between 0-100: "); // asks for new input 
    boolean flag = false; // sets to false so it will go through while loop 
    int input = 0; // initializes variable 
    
    while (!flag){ //make a loop statement for if they do not enter a positive integer
      if (myScanner.hasNextInt()){
        input = myScanner.nextInt();
        flag = true;
        if (input > 0 && input < 100){ // if valued entered is between the value it will set flag to true so that it exits the while loop 
             flag = true;
        }

        else {
             System.out.print ("Error. please type in an integer between 0-100: "); // asks for value between 
             flag = false; // sets false so it check if it is in value 
        }

      }

      else {
        myScanner.next(); // discards value 
        System.out.print("Error. please type in an integer: "); // asks them to enter a new value 
        flag = false; // sets to false so it runs through loop again 

      }
    }

 
  // if i is less than the output it will go to the next line 
  for (int i = 0; i < input; i++){
    // System.out.println(); // new line 
    for (int j = 0; j < input; j++){ // when j is less than the output 
      if(i == j || j ==(input - i -  1)){
        System.out.print(" "); // prints spaces in an X form 
     
      }
      else {
        System.out.print("*"); // prints starts 
        
      }
      
    }
     
    System.out.println(); // prints new line 
  }
    
    
  }
  
  
  
}