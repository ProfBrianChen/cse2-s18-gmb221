



/* Georgia Burlingame 
CSE 2
HW 8
04/10/2018 */

import java.util.Scanner; // imports scanner 
import java.util.Random; // imports random number generator

public class CSE2Linear {
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); // declares scanner
    System.out.println("Enter 15 ascending ints for final grades in CSE2: "); // asks user to input values 
    int enteredNumbers = 0; // intializes variable 
    boolean flag = false; // creates boolean for error 
    int [] gradesEntered = new int[15]; // creates array 
     
    for(int i = 0; i < gradesEntered.length; i++){ // checks all of the numbers entered in the array
      flag = false;
      while(!flag){ // if true 
        if(myScanner.hasNextInt()){ // checks to see if an int was measured 
          enteredNumbers = myScanner.nextInt(); // accepts the int if measured 
          if(0<= enteredNumbers && enteredNumbers <= 100){// makes sure numbers entered are between 0 and 100
            if((i>0 && (gradesEntered[i-1] <= enteredNumbers)) || i == 0)
              flag = true; // sets to true so that it will stop going through while loop  
            else{
               System.out.print("ERROR! Numbers entered in array should go in ascending order "); //tells user to put the int in increasing order
               flag = false; // sets to false so it will go through while loop again 
            }
         
          }
          
          else{
            System.out.print("ERROR! Please type in a number between 0 - 100 "); // enter in new numbers 
            flag = false; // sets to false so it will go through while loop again 
          }
        }
        
        else{
          myScanner.next(); //discards the numbers entered 
          System.out.print("ERROR! Please enter type int "); // if not a type int then it will keep going through the loop 
          flag = false; // sets to false so it will go through while loop again 
        }
      }
      gradesEntered[i] = enteredNumbers; // put the numbers you entered into the array 
    }
    
    printArray(gradesEntered); // prints the array you entered 
   
    
  
    binarySearch(gradesEntered); //calls method
    
    
    scrambleArray(gradesEntered); //calls method
    
    
    linearSearch(gradesEntered); // calls method 
  }
  
  public static void printArray(int[] arrays){
    for(int i = 0; i < arrays.length; i++){
      System.out.println(arrays[i] + " ");// prints out the array 
      
      
    }
    System.out.println();
  }
  
  
  public static void scrambleArray(int[] array){
    Random randomGenerator = new Random(); // generates random numbers
   System.out.println("Scrambled: ");
    for (int i = 0; i < array.length; i++){ // for loop for shuffling array 
       int randomNumber = (int) (array.length * Math.random()); // random numbers for the length of the array 
      int swap = array[randomNumber]; // shuffles array 
      array[randomNumber] = array[i]; // shuffles array 
      array[i] = randomNumber; // shuffles array 
     System.out.print(array[i] + " "); //prints scrambled array 
    }
    System.out.println(); //prints new line
  }
  
  public static void linearSearch (int[] array2){
    
    Scanner myScanner = new Scanner(System.in); // declares new scanner 
    System.out.print("Enter a grade to search for: "); // asks user to enter a value 
    int gradeToSearch = myScanner.nextInt(); // accepts value entered
    //int interations = 0;
    int count = 1; // sets counter 
    
    for (int i = 0; i < array2.length; i++){
      if(gradeToSearch == array2[i]){ // if the value entered is equal to the array at the value 
         //return true;
        System.out.println(gradeToSearch + " was found in the list with " + count + " interations" ); // prints statement 
        break; // breaks out of loop 
      }
      
      else if (i == 14 && gradeToSearch != array2[14] ){
       //return false;
        
        System.out.println(gradeToSearch + " was not found in the list with " + count + " interations" ); // prints statement 
        break; // breaks out of loop 
      }
      count = count + 1; //counts iterations 
      
    }
   }
  
   
 
    public static void binarySearch (int[] array1){
    
      Scanner myScanner = new Scanner(System.in); // declares new scanner 
      System.out.print("Enter a grade to search for: ");
      int num = myScanner.nextInt(); // accepts number as int
      int first = 0; // intializes variable 
      int last = 14; // initializes variable 
   
    
    while(last > first){
      int middle = (first + last)/2;
      if (array1[middle] == num){ // if number is in the area
        
        System.out.println(num + " was found in the list"); // if the number you entered is in the array 
        break;
      }
      
      else if(array1[middle] < num){ 
        first = middle + 1; // if the num is greater than the middle it will keep adding on to see if value is in there 
      }
      else if(array1[middle] > num){
        last = middle - 1; // if the middle number is greater than the number you entered it will subtract to see if num you entered is in there
      } 
      
      else{
      
        System.out.println(num + " was not found in the list"); // if the number you entered is not in the array 
        break;
      }
    }
   }
  
}

