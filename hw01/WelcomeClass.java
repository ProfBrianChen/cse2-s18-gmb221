

/// CSE 2 Welcome Class HW01 Georgia Burlingame///

//prints out dashes and a welcome statement including my lehigh user id ///


public class WelcomeClass {
    public static void main(String[] args){
      //Prints "Welcome" to the terminal window
      System.out.println("  -----------");
      System.out.println("  | WELCOME |");
      System.out.println("  -----------");
      System.out.println("  ^  ^  ^  ^  ^  ^  ");
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-G--M--B--2--2--1-> ");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");
                         
      
    }
}