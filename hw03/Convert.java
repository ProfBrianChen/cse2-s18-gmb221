// hw03 
// Georgia Burlingame 
// 02.13.2018

// purpose is to obtain an input from a user on the number of acres of land 
// and inches of water to calculate the quantity of rain into cubic miles 


// creates an input that inputs can be taken from the user
import java.util.Scanner;

// creates a class
public class Convert{
  
  // creates a string
  public static void main(String[] args){
    
    // declares scanner
    Scanner myScanner = new Scanner(System.in);
    
    //asks the user to input a value
    System.out.print("Enter the affected area in acres: ");
    
    //accepts the input from the user
    double acresPrecip = myScanner.nextDouble(); 
    
    // asks the user to input a value
    System.out.print("Enter the rainfall in the affected area: ");
    
    // accepts the input from the user
    double rainfallInches = myScanner.nextDouble();
    
    // initializes a variable as a double 
    double acreInches;
    
    //multiplies acres of land by inches of rain to get acre inches
    acreInches = acresPrecip*rainfallInches;
    
    // initializes a variable as a double 
    double gallonsRain;
    
    // calculates the gallons of rain by multipling it by the amount of gallons in one acre inch 
    gallonsRain = acreInches*27154.285714286;
    
    // creates a variable that calculates the cubic miles 
    float cubicMileRain = (float) (gallonsRain*(9.08169e-13));
    
    
    // prints out the cubic miles of rain 
    System.out.println(cubicMileRain + " cubic miles");
    
    
  }
}