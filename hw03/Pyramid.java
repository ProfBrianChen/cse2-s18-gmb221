// hw03 
// Georgia Burlingame 
// 02.13.2018

// calculates the volume of a pyramid given the the length of square side and the height

// allows you to create input statements 
import java.util.Scanner;

// creates class for the code
public class Pyramid{
  
  // creates a sting
  public static void main(String[] args){
    
    // declares scanner
    Scanner myScanner = new Scanner(System.in);
    
    // asks the user to input a value
    System.out.print("The square side of the pyramid is (input length): ");
    
    // accepts the input from the user
    double squareSideLength = myScanner.nextDouble(); 
    
    // asks the user to input another value 
    System.out.print("The height of the pyramid is (input height): ");
    
    // accepts the input from the user
    double height = myScanner.nextDouble();
    
    // intializes a variable as a double 
    double volumePyramid; 
    
    // calculates the volume of the pyramid by multiplying the side length twice to get the area of the base and then multiplying it by the height to get the volume 
    volumePyramid = (squareSideLength * squareSideLength * height)/3;
    
    // prints the volume the pyramid 
    System.out.println("The volume inside the pyramid is: " + volumePyramid);
    
  }
}
