// CSE 2 Lab 03
// Georgia Burlingame 
// 02.09.2018
// Check

// code will obtain an input from the user of the cost of the check
// obtain an input from the user of the percentage tip the want to pay 
// obtain an input from the user of the # of ways they will split the check 
// last, determine how much each person will need to pay 


// creates an import that inputs can be taken from the user
import java.util.Scanner;

// creates a class 
public class Check{
  
 
  // initializes a string
   public static void main(String[] args){
     
     // declares scanner
     Scanner myScanner = new Scanner(System.in);
     
     // asks the user to input a value 
     System.out.print("Enter the original cost of the check in the form xx.xx: ");
     
     // accepts the input from the user
     double checkCost = myScanner.nextDouble();
     
     // asks for an input from the user 
     System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); 
     
     double tipPercent = myScanner.nextDouble();  // accepts the second input from the user
     
     tipPercent /= 100; //converts to a decimal value
       
     System.out.print("Enter the number of people who went out to dinner: "); // asks user for a number 
     int numPeople = myScanner.nextInt(); // accepts the input as an integer
     
     double totalCost; // initializes a variable  as a double 
     double costPerPerson; // initializes a variable as a double 
     int dollars; // converts into a whole dollar amount 
     int dimes; //  creates an integer for the number for decimal places 
     int pennies; // creates an integer for the number of pennies which will be used for the decimal
     
   
     totalCost = checkCost * (1 + tipPercent); // calculates total cost with tip 
     costPerPerson = totalCost/numPeople; // determines how much each person has to pay by taking total cost and dividing by number of people inputed earlier
     
     dollars = (int) costPerPerson; // turns the cost into a whole number so that it can add the dimes and pennies 
     
     dimes = (int) (costPerPerson * 10) % 10; // calculates the amount of dimes that will be used as a decimal 
     pennies = (int) (costPerPerson * 100) % 10; // calculates the amount of pennies for the decimal 
     
     System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); // prints how much each person owes, adds dimes and pennies 
     
     
     
   } // end of main method 
  
  
} // end of class