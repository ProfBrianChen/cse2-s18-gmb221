// Georgia Burlingame
// CSE 2
// Lab 04
// 02.16.2018
// Card Generator 

// Purpose of this lab is to generate a random number 

import java.util.Random; 

// multiply by 52, cast it as an integer and add 1

// create a class
public class CardGenerator{
  
 
  // create a string 
  public static void main(String[] args){
    
    // Creates the deck of cards ranging from numbers 1 to 52
    Random rand = new Random();
    
    // creates a integer from 1 to 52
    int cardNumber = (int)(Math.random()*52) + 1; 
    
    
    String suits; // creates a variable for suits as a string that will be used for print statement 

    
    // each if statement determines which suit the cards represent
    // creates an if statement for the randomly number a range of numbers that represent a certain suit
    if (cardNumber >= 1 && cardNumber <= 13) {
      
      
      // establishes Diamonds as the suit for these random numbers 
      suits = "Diamonds";
    
     // creates a swith that divdes the number of cards by 13 and the remainder number corresponds to a face value
      switch (cardNumber % 13){
         // each case prints out the face of the card and the suit that it is 
        case 1: System.out.println("You got a Ace of " + suits); break;
        case 2: System.out.println("You got a 2 of " + suits); break;
        case 3: System.out.println("You got a 3 of " + suits); break;
        case 4: System.out.println("You got a 4 of " + suits); break;
        case 5: System.out.println("You got a 5 of " + suits); break;
        case 6: System.out.println("You got a 6 of " + suits); break;
        case 7: System.out.println("You got a 7 of " + suits); break;
        case 8: System.out.println("You got a 8 of " + suits); break;
        case 9: System.out.println("You got a 9 of " + suits); break;
        case 10: System.out.println("You got a 10 of " + suits); break;
        case 11: System.out.println("You got a Jack of " + suits); break;
        case 12: System.out.println("You got a Queen of " + suits); break;
        case 13: System.out.println("You got a King of " + suits); break;
        
    }

 }
    
    // each if statement determines which suit the cards represent
    // creates an else if statement for the numbers a range of numbers that represent a certain suit 
    else if (cardNumber > 13 && cardNumber <= 26){
      
      
      // establishes Clubs as the suit for these random numbers 
      suits = "Clubs";
  
      // creates a swith that divdes the number of cards by 13 and the remainder number corresponds to a face value
      switch (cardNumber % 13){
         // each case prints out the face of the card and the suit that it is 
        case 1: System.out.println("You got a Ace of " + suits); break;
        case 2: System.out.println("You got a 2 of " + suits); break;
        case 3: System.out.println("You got a 3 of " + suits); break;
        case 4: System.out.println("You got a 4 of " + suits); break;
        case 5: System.out.println("You got a 5 of " + suits); break;
        case 6: System.out.println("You got a 6 of " + suits); break;
        case 7: System.out.println("You got a 7 of " + suits); break;
        case 8: System.out.println("You got a 8 of " + suits); break;
        case 9: System.out.println("You got a 9 of " + suits); break;
        case 10: System.out.println("You got a 10 of " + suits); break;
        case 11: System.out.println("You got a Jack of " + suits); break;
        case 12: System.out.println("You got a Queen of " + suits); break;
        case 13: System.out.println("You got a King of " + suits); break;
      
      }
   }
   
    
    // each if statement determines which suit the cards represent
    // creates an else if statement for the numbers a range of numbers that represent a certain suit
    else if (cardNumber > 26 && cardNumber <= 39){
      
      // establishes Hearts as the suit for these random numbers 
      suits = "Hearts";
     
      // creates a swith that divdes the number of cards by 13 and the remainder number corresponds to a face value
      switch (cardNumber % 13){
         // each case prints out the face of the card and the suit that it is 
        case 1: System.out.println("You got a Ace of " + suits); break;
        case 2: System.out.println("You got a 2 of " + suits); break;
        case 3: System.out.println("You got a 3 of " + suits); break;
        case 4: System.out.println("You got a 4 of " + suits); break;
        case 5: System.out.println("You got a 5 of " + suits); break;
        case 6: System.out.println("You got a 6 of " + suits); break;
        case 7: System.out.println("You got a 7 of " + suits); break;
        case 8: System.out.println("You got a 8 of " + suits); break;
        case 9: System.out.println("You got a 9 of " + suits); break;
        case 10: System.out.println("You got a 10 of " + suits); break;
        case 11: System.out.println("You got a Jack of " + suits); break;
        case 12: System.out.println("You got a Queen of " + suits); break;
        case 13: System.out.println("You got a King of " + suits); break;
        
    }
      
  }
    
    // each if statement determines which suit the cards represent
    // creates an if statement for the numbers a range of numbers that represent a certain suit
    else if (cardNumber > 39 && cardNumber <= 52){
      
      // establishes Spades as the suit for these random numbers 
      suits = "Spades";
      
      
      // creates a swith that divdes the number of cards by 13 and the remainder number corresponds to a face value
      switch (cardNumber % 13){
        // each case prints out the face of the card and the suit that it is 
        case 1: System.out.println("You got a Ace of " + suits); break; 
        case 2: System.out.println("You got a 2 of " + suits); break;
        case 3: System.out.println("You got a 3 of " + suits); break;
        case 4: System.out.println("You got a 4 of " + suits); break;
        case 5: System.out.println("You got a 5 of " + suits); break;
        case 6: System.out.println("You got a 6 of " + suits); break;
        case 7: System.out.println("You got a 7 of " + suits); break;
        case 8: System.out.println("You got a 8 of " + suits); break;
        case 9: System.out.println("You got a 9 of " + suits); break;
        case 10: System.out.println("You got a 10 of " + suits); break;
        case 11: System.out.println("You got a Jack of " + suits); break;
        case 12: System.out.println("You got a Queen of " + suits); break;
        case 13: System.out.println("You got a King of " + suits); break;
        
    }
    
 }
          
        
    }
  
  
}


