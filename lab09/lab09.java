/* georgia burlingame 
cse2
chen 
04/13/2018 */

import java.util.Scanner; 

public class lab09{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
     System.out.print("Input an the size of array that you want "); 
    int size = myScanner.nextInt();
    int[] array0 = new int[size]; // creates an array of the size the user asked for 

    
    // takes the length that the user wants and creates and array0 of that length 
    for (int i = 0; i < array0.length; i++){
      array0[i] = i; 
    }
    

    
    // makes two copies of the array0 
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
     
    inverter(array1); 
    int[] array3 =  inverter2(array2);
    printArray(array0);
    printArray(array1);
    printArray(array2);
    printArray(array3);
  }
  
  //method to print the array 
  public static void printArray(int[] arrayToPrint){
    for(int i = 0; i< arrayToPrint.length; i++){
      System.out.print(arrayToPrint[i] + " ");
    }
    System.out.println();
  }
  
  // copies the array from the main method and respective methods when this method is called 
  public static int[]  copy (int[] a){
    int[] copiedArray = new int[a.length]; // creates a new array that is the same length as a which is array0 in the main method
    for(int i = 0; i < copiedArray.length; i++){
      copiedArray[i] = a[i];
    }
  return copiedArray; // returns the copied array 
  }
  
 public static void inverter(int[] b){
   
   for(int i = 0; i < b.length/2; i++){
     int temp = b.length - 1 - i;
     int end = b[temp];
     int front = b[i];
     b[i] = end;
     b[temp] = front; 
   }
 
 }
  
  public static int[] inverter2(int[] c){
    int[] array2 = copy(c); // copies the array 
    inverter(array2);
    return array2;
  }
  
}