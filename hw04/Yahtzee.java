// Georgia Burlingame
// CSE 2
// 02.20.2018
// HW 04
// Yahtzee 

// Purpose is to create a program that runs the game Yahtzee

// imports a random number generator 
import java.util.Random;
import java.util.Scanner;

// declares class
public class Yahtzee{
  
  // declares string 
  public static void main(String[] args){
    
    
    //starts a new scanner
    Scanner myScanner = new Scanner(System.in);
    
    int dieOne = (int) (Math.random()*6) + 1; //give number between 1 and 6 for die #1
    int dieTwo = (int) (Math.random()*6) + 1; //give number between 1 and 6 for die #2
    int dieThree = (int) (Math.random()*6) + 1; //give number between 1 and 6 for die #3
    int dieFour = (int) (Math.random()*6) + 1; //give number between 1 and 6 for die #4
    int dieFive = (int) (Math.random()*6) + 1; //give number between 1 and 6 for die #5

    
    System.out.print("Choose 1 if you want to pick the number that you roll for your die. Choose 2 if you want to randomly roll 5 die ");
    int choice = myScanner.nextInt(); // makes it an integer
   
    
    // if you choose 1
    if (choice == 1) {
      System.out.print("Please enter a 5 digit number. Each digit will represent the face value of the die ");
      System.out.print(" ");
      int fiveDigits = myScanner.nextInt(); // accepts the answer as an integer 
      
     
      
      // divides out the number so that each value is represents the face value of the die 
      dieOne = fiveDigits/10000 % 10; 
      dieTwo = fiveDigits/1000 % 10; 
      dieThree = fiveDigits/100 % 10; 
      dieFour = fiveDigits/10 % 10; 
      dieFive = fiveDigits % 10; 
      
 
       
    }
    
    // if you choose 2
    else if (choice == 2){
      // uses the random generated numbers 
    
    }
    
    // if you chose neither 1 nor 2
    else {
      System.out.println("ERROR! You did not choose a valid option");
    }
    
    // prints face value that was rolled for each die 
    System.out.println("Die one: " + dieOne);
    System.out.println("Die two: " + dieTwo);
    System.out.println("Die three: " + dieThree);
    System.out.println("Die four: " + dieFour);
    System.out.println("Die five: " + dieFive);
    
    // sets each possible face value to one so that it will be able to count how many die have a certain face value in switch statement 
    int aces = 0; 
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    
    // checks the face value of a die and adds 1 to it so it counts it 
    // meaning if dieOne = 4, dieTwo = 5, dieThree = 4, dieFour = 1, dieFive = 2, after going through the switch statements: aces = 1, twos = 1, threes = 0, fours = 2, fives = 1, sixes = 0
    switch (dieOne){
      case 1: aces = aces + 1; break;
      case 2: twos = twos + 1; break; 
      case 3: threes = threes + 1; break; 
      case 4: fours  = fours + 1; break;
      case 5: fives = fives + 1; break;
      case 6: sixes = sixes + 1; break; 
      
    }
    
     switch (dieTwo){
      case 1: aces = aces + 1; break;
      case 2: twos = twos + 1; break; 
      case 3: threes = threes + 1; break; 
      case 4: fours  = fours + 1; break;
      case 5: fives = fives + 1; break;
      case 6: sixes = sixes + 1; break; 
      
    }
    
    
    switch (dieThree){
      case 1: aces = aces + 1; break;
      case 2: twos = twos + 1; break; 
      case 3: threes = threes + 1; break; 
      case 4: fours  = fours + 1; break;
      case 5: fives = fives + 1; break;
      case 6: sixes = sixes + 1; break; 
    }
    
    switch (dieFour){
      case 1: aces = aces + 1; break;
      case 2: twos = twos + 1; break; 
      case 3: threes = threes + 1; break; 
      case 4: fours  = fours + 1; break;
      case 5: fives = fives + 1; break;
      case 6: sixes = sixes + 1; break; 
      
    }
    
    switch (dieFive){
      case 1: aces = aces + 1; break;
      case 2: twos = twos + 1; break; 
      case 3: threes = threes + 1; break; 
      case 4: fours  = fours + 1; break;
      case 5: fives = fives + 1; break;
      case 6: sixes = sixes + 1; break; 
    }
    
    // calculates the total value for have that number of each face value 
  
    int acesScore = aces * 1;
    int twosScore = twos * 2;
    int threesScore = threes * 3;
    int foursScore = fours * 4;
    int fivesScore = fives * 5;
    int sixesScore = sixes * 6;
    
    //prints upper section 
    System.out.println(" ");
    System.out.println("UPPER SECTION");
   
    
    // prints out the number of each value you have
    System.out.println("Aces total: " + acesScore);
    System.out.println("Twos total: " + twosScore);
    System.out.println("Threes total: " + threesScore);
    System.out.println("Fours total: " + foursScore);
    System.out.println("Fives total: " + fivesScore);
    System.out.println("Sixes total: " + sixesScore);
    
    
    
    
    // calculates total by adding values of all the faces scored
    int totalUpperSection = acesScore + twosScore + threesScore + foursScore + fivesScore + sixesScore;
    System.out.println("Total = " + totalUpperSection); // prints this value calcuated
    
    
    
    
    
    
    int bonus = 35; // sets value of bonus to 35 as an integer 
    int totalUpperSectionBonus = (totalUpperSection + bonus); // calcuates total w/ bonus for upper sectio 
    
    // determines if a bonus will be added based on what the total upper section score was
    if (totalUpperSection >= 63){
      System.out.println("Total with Bonus = " + totalUpperSectionBonus); // prints the value w/ bonus 
      
    }
    
   
    
    // greats the lower section to print
    System.out.println(" ");
    System.out.println("LOWER SECTION");
  
    
    
    
    
    
    
    
    // establishes the values for 3 of a kind 
    int threeKind;
    int threeKindAces = 3; // 3 ones score will be 3
    int threeKindTwos = 6; // 3 twos score will be 6
    int threeKindThrees = 9; // 3 thress score will be 9
    int threeKindFours = 12; // 3 fours score will be 12 
    int threeKindFives = 15; // 3 fives score will be 15
    int threeKindSixes = 18; // 3 sixes score will be 18
    
         // determines if the dice rolled are 3 of a kinds
        // what all the possible combinations for all the having 3 die have 1s
    if (dieOne == 1 && dieTwo == 1 && dieThree == 1 || dieOne == 1 && dieTwo == 1 && dieFour == 1
        || dieOne == 1 && dieTwo == 1 && dieFive == 1 || dieTwo == 1 && dieThree == 1 && dieFour == 1
        || dieTwo == 1 && dieThree == 1 && dieFive == 1 || dieThree == 1 && dieFour == 1 && dieFive == 1){

      System.out.println("Three of a Kind Score = " + threeKindAces); //prints score
      threeKind = threeKindAces; // sets score to this which will be used down below for calculation of lower section total 

    }
    
        
        // what all the possible combinations for all the having 3 die have 2s
    else if (dieOne == 2 && dieTwo == 2 && dieThree == 2 || dieOne == 2 && dieTwo == 2 && dieFour == 2
        || dieOne == 2 && dieTwo == 2 && dieFive == 2 || dieTwo == 2 && dieThree == 2 && dieFour == 2
        || dieTwo == 2 && dieThree == 2 && dieFive == 2 || dieThree == 2 && dieFour == 2 && dieFive == 2){

      System.out.println("Three of a Kind Score = " + threeKindTwos); //prints score 
      threeKind = threeKindTwos; // sets score to this which will be used down below for calculation of lower section total 

    }

   
        // what all the possible combinations for all the having 3 die have 3s 
     else if (dieOne == 3 && dieTwo == 3 && dieThree == 3 || dieOne == 3 && dieTwo == 3 && dieFour == 3
        || dieOne == 3 && dieTwo == 3 && dieFive == 3 || dieTwo == 3 && dieThree == 3 && dieFour == 3
        || dieTwo == 3 && dieThree == 3 && dieFive == 3 || dieThree == 3 && dieFour == 3 && dieFive == 3){

      System.out.println("Three of a Kind Score = " + threeKindThrees); //prints score 
       threeKind = threeKindThrees; // sets score to this which will be used down below for calculation of lower section total 

    }

   
      // what all the possible combinations for all the having 3 die have 4s 
     else if (dieOne == 4 && dieTwo == 4 && dieThree == 4 || dieOne == 4 && dieTwo == 4 && dieFour == 4
        || dieOne == 4 && dieTwo == 4 && dieFive == 4 || dieTwo == 4 && dieThree == 4 && dieFour == 4
        || dieTwo == 4 && dieThree == 4 && dieFive == 4 || dieThree == 4 && dieFour == 4 && dieFive == 4){

      System.out.println("Three of a Kind Score = " + threeKindFours); // prints score 
       threeKind = threeKindFours; // sets score to this which will be used down below for calculation of lower section total 

    }

   

     else if (dieOne == 5 && dieTwo == 5 && dieThree == 5 || dieOne == 5 && dieTwo == 5 && dieFour == 5
        || dieOne == 5 && dieTwo == 5 && dieFive == 5 || dieTwo == 5 && dieThree == 5 && dieFour == 5
        || dieTwo == 5 && dieThree == 5 && dieFive == 5 || dieThree == 5 && dieFour == 5 && dieFive == 5){

      System.out.println("Three of a Kind Score = " + threeKindFives); // prints score 
       threeKind = threeKindFives; // sets score to this which will be used down below for calculation of lower section total 

    }

   

     else if (dieOne == 6 && dieTwo == 6 && dieThree == 6 || dieOne == 6 && dieTwo == 6 && dieFour == 6
        || dieOne == 6 && dieTwo == 6 && dieFive == 6 || dieTwo == 6 && dieThree == 6 && dieFour == 6
        || dieTwo == 6 && dieThree == 6 && dieFive == 6 || dieThree == 6 && dieFour == 6 && dieFive == 6){

      System.out.println("Three of a Kind Score = " + threeKindSixes); // prints your score 
       threeKind = threeKindSixes; // sets score to this which will be used down below for calculation of lower section total 

    }
    
    else {
       threeKind = 0;
      System.out.println("Three of a Kind Score = " + threeKind); // if no 3 of a kind was detected
    
    }
    
    
    
    
    
    
    
    
    // determines the value if there is a 4 of a kind for each face value 
    int fourKind;
    int fourKindAces = 4; // 4 ones will give you a score of 4
    int fourKindTwos = 8; // 4 twos will give you a score of 8
    int fourKindThrees = 12; // 4 threes will give you a score of 12
    int fourKindFours = 16; // 4 fours will give you a score of 16
    int fourKindFives = 20; // 4 fives will give you a score of 20
    int fourKindSixes = 24; // 4 sixes will give you a score of 24
    
       // all possible combinations for 4 of a kinds w/ face value of 1
       if (dieOne == 1 && dieTwo == 1 && dieThree == 1 && dieFour == 1
        || dieOne == 1 && dieTwo == 1 && dieThree == 1 && dieFive == 1
        || dieThree == 1 && dieTwo == 1 && dieFive == 1 && dieFour == 1
        || dieTwo == 1 && dieOne == 1 && dieFour == 1 && dieFive == 1){

      System.out.println("Four of a Kind Score = " + fourKindAces); //prints score 
         fourKind = fourKindAces;  // sets score to this which will be used down below for calculation of lower section total 
         

    }

   
         // all possible combinations for 4 of a kinds w/ face value of 2
    else if (dieOne == 2 && dieTwo == 2 && dieThree == 2 && dieFour == 2
        || dieOne == 2 && dieTwo == 2 && dieThree == 2 && dieFive == 2
        || dieThree == 2 && dieTwo == 2 && dieFive == 2 && dieFour == 2
        || dieTwo == 2 && dieOne == 2 && dieFour == 2 && dieFive == 2){

      System.out.println("Four of a Kind Score = " + fourKindTwos); //prints score 
      fourKind = fourKindTwos;  // sets score to this which will be used down below for calculation of lower section total 

    }

   
         // all possible combinations for 4 of a kinds w/ face value of 3
     else if (dieOne == 3 && dieTwo == 3 && dieThree == 3 && dieFour == 3
        || dieOne == 3 && dieTwo == 3 && dieThree == 3 && dieFive == 3
        || dieThree == 3 && dieTwo == 3 && dieFive == 3 && dieFour == 3
        || dieTwo == 3 && dieOne == 3 && dieFour == 3 && dieFive == 3){

      System.out.println("Four of a Kind Score = " + fourKindThrees); // prints score
       fourKind = fourKindThrees; // sets score to this which will be used down below for calculation of lower section total 

    }

   
          // all possible combinations for 4 of a kinds w/ face value of 4
     else if (dieOne == 4 && dieTwo == 4 && dieThree == 4 && dieFour == 4
        || dieOne == 4 && dieTwo == 4 && dieThree == 4 && dieFive == 4
        || dieThree == 4 && dieTwo == 4 && dieFive == 4 && dieFour == 4
        || dieTwo == 4 && dieOne == 4 && dieFour == 4 && dieFive == 4){

      System.out.println("Four of a Kind Score = " + fourKindFours); //prints score 
       fourKind = fourKindFours;  // sets score to this which will be used down below for calculation of lower section total 

    }

   
        // all possible combinations for 4 of a kinds w/ face value of 5
     else if (dieOne == 5 && dieTwo == 5 && dieThree == 5 && dieFour == 5
        || dieOne == 5 && dieTwo == 5 && dieThree == 5 && dieFive == 5
        || dieThree == 5 && dieTwo == 5 && dieFive == 5 && dieFour == 5
        || dieTwo == 5 && dieOne == 5 && dieFour == 5 && dieFive == 5){

      System.out.println("Four of a Kind Score = " + fourKindFives); //prints score 
       fourKind = fourKindFives; // sets score to this which will be used down below for calculation of lower section total 

    }

   
      // all possible combinations for 4 of a kinds w/ face value of 6
     else if (dieOne == 6 && dieTwo == 6 && dieThree == 6 && dieFour == 6
        || dieOne == 6 && dieTwo == 6 && dieThree == 6 && dieFive == 6
        || dieThree == 6 && dieTwo == 6 && dieFive == 6 && dieFour == 6
        || dieTwo == 6 && dieOne == 6 && dieFour == 6 && dieFive == 6){

      System.out.println("Four of a Kind Score = " + fourKindSixes); //prints score 
       fourKind = fourKindSixes; // sets score to this which will be used down below for calculation of lower section total 

    }
    
    //for when none of the above condition are true 
    else{
      fourKind = 0;  // what score is if there is no 4 of a kind 
      System.out.println("Four of a Kind Score = " + fourKind); // prints that the value they get is 0 for

    }
  
    
    
    
    
    
    
    
    int fullHouse = 25; // creates an int for the full house score 
    
    
    // determine if the rolled die make up a full house, determines all the possible combinations to check for 3 die with the same value and 2 die with a different value 
    if (dieOne == dieTwo && dieThree == dieFour && dieFour == dieFive
       || dieOne == dieThree && dieTwo == dieFour && dieFour == dieFive
       || dieOne == dieFour && dieTwo == dieThree && dieThree == dieFive
       || dieOne == dieFive && dieTwo == dieThree && dieThree == dieFour
       || dieTwo == dieThree && dieFour == dieFive && dieFive == dieOne
       || dieTwo == dieFour && dieOne == dieThree && dieThree == dieFive
       || dieTwo == dieFive && dieOne == dieThree && dieThree == dieFour
       || dieThree == dieFour && dieOne == dieTwo && dieTwo == dieFive
       || dieThree == dieFive && dieOne == dieTwo && dieTwo == dieFour
       || dieFour == dieFive && dieOne == dieTwo && dieTwo == dieThree){
      
      System.out.println("Full House Score " + fullHouse); //prints the score if one of the statements above is true
    }
    
    else{
      fullHouse = 0;
      System.out.println("Full House Score = " + fullHouse); //if there is no full house
    }
    
    
    
    
    
    
    
    
    int smallStraight = 30; // sets score you will get with a small straight 
    
    // determines if there is a small straight 
    if (acesScore > 0 && twosScore > 0 && threesScore > 0 && foursScore > 0 
        || twosScore > 0 && threesScore > 0 && foursScore > 0 && fivesScore > 0
        || threesScore > 0 && foursScore > 0 && fivesScore > 0 && sixesScore > 0){
      
      System.out.println("Small Straight Score = " + smallStraight); // prints out small striaght score 
      
    }
    
    else{
      smallStraight = 0;
      System.out.println("Small Straight Score = " + smallStraight); // if you do not have a small straight 
      
    }
    
    int largeStraight = 40; // score for a large straight 
    
    // determines if it is a large straight 
    if (acesScore > 0 && twosScore > 0 && threesScore > 0 && foursScore > 0 && fivesScore > 0
       || twosScore > 0 && threesScore > 0 && foursScore > 0 && fivesScore > 0 && sixesScore > 0){
    
      
      System.out.println("Large Straight Score = " + largeStraight); // prints large straight score 
  
    }
    
    else {
      largeStraight = 0; 
      System.out.println("Large Straight Score = " + largeStraight); // if no large straight
      
    }
    
    
    
    
    
    
    
    int yahtzee = 50; // sets score if there is a yahtzee
    
    // determines if there is a yahtzee by seeing if all dies equal each other 
    if (dieOne == dieTwo && dieTwo == dieThree && dieThree == dieFour && dieFour == dieFive){
      
      System.out.println("Yahtzee = " + yahtzee); // prints if a yahtzee 
    }
    
    else {
      System.out.println("Yahtzee = 0"); // if all of the die aren't the same number then not a yahtzee so score for it is 0 
      
    }
    
    
    
    
    
    
    
    int chance; // creates variable for chance 
    chance = totalUpperSection; // calculates the value of chance 
    System.out.println("Chance = " + chance); // prints the value of chance 
  
    
    
    int totalLowerSection;  // creates variable for lower section 
    totalLowerSection = threeKind + fourKind + fullHouse + smallStraight + largeStraight + yahtzee + chance; // calculates lower total 
    
    
    int grandTotal; // creates a variable for grand total 
    grandTotal = totalLowerSection + totalUpperSection; // calculates grand total 
    
    
    
    System.out.println("Lower Total = " + totalLowerSection); // prints lower total 
    System.out.println("Grand Total = " + grandTotal); // prints grandTotal 
 
  }
  
  
}
  
    