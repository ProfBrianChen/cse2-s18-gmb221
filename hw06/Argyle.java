// Georgia Burlingame
// CSE 2 
// Hw 06 

import java.util.Scanner;

public class Argyle{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); // new scanner for the width 
    System.out.print ("Enter a positive interger for the width of Viewing window in characters: "); //asks the user to input a positive integer
    int width = 0; //this is how they will input integer
    boolean flag = false; // declares flag as a boolean 

    while (!flag){ //make a loop statement for if they do not enter a positive integer
      // if the value entered is an integer 
      if (myScanner.hasNextInt()){
        width = myScanner.nextInt(); // accepts the value as an integer 
        flag = true; // sets flag to true 
        // if the width is positive 
        if (width > 0){
          // accepts the value 
             flag = true;
        }
        // if the value is NEGATIVE 
        else {
             //asks for the user to input a positive integer 
             System.out.print ("Error: please type in a positive integer ");
            // sets flag to false so that it goes again through the loop 
             flag = false;
        }
      }
      // if the value is NOT an integer 
      else {
        
        myScanner.next(); // discards the value 
        System.out.print("Error: please type in an integer "); // asks the user to input another value 
        flag = false; // sets flag to false so that it will run through the value again 

      }
    }
  
    Scanner height = new Scanner(System.in); // sets a new scanner for height 
    System.out.print("Enter a positive integer for the height of Viewing window in characters: "); // asks user to input a value for the height 
    boolean flag2 = false; // sets a boolean for the while loop 
    int heightOfWindow = 0; // initializes variable 
    
    while (!flag2){ //make a loop statement for if they do not enter a positive integer
      // if the value is an integer 
      if (myScanner.hasNextInt()){
        heightOfWindow = myScanner.nextInt(); // accepts the value as an integer 
        flag2 = true; // sets flag to true so it will stop running through loop 
        
        // if the value entered is positive 
        if (heightOfWindow > 0){
             flag2 = true; // sets flag to true so that it will exit the while loop 
        }
        
        
        // if the value is negative 
        else {
             System.out.print ("Error: please type in a positive integer "); // asks the user to input another a new value 
             flag2 = false; // sets flag to false so that it will go through the while loop again to check if it is a positive value and if it is an integer 
        }
      }
      

      else {
        myScanner.next(); // discards value that was entered 
        System.out.print("Error: please type in an integer "); // asks user to enter a new value 
        flag2 = false; // sets flag to false so it will go through the while loop again 

      }
    }
    
    Scanner widthDiamonds = new Scanner(System.in); // new scanner
    System.out.print("Enter a positive integer for the width of the argyle diamonds: "); // ask user to input a value 
    boolean flag3 = false; // declares a variable 
    int widthofDiamondsWindow = 0; // initializes varible 
    
    // 
    while (!flag3){ //make a loop statement for if they do not enter a positive integer
      // if the value is an integer 
      if (myScanner.hasNextInt()){
        widthofDiamondsWindow = myScanner.nextInt(); // accepts the value as an integer 
        flag3 = true; // sets to true 
        //if value entered is positive 
        if (widthofDiamondsWindow > 0){
             flag3 = true; //sets value to true and breaks out of the while loop 
        }
        
        // if the value is not positive 
        else {
             System.out.print ("Error: please type in a positive integer "); // asks user to input a new value 
             flag3 = false; // sets to false to go through while loop again and check it if is an an integer and if it is positive
        }
      }
      // if value is not an integer 
      else {
        myScanner.next(); // discards previously entered value 
        System.out.print("Error: please type in an integer "); // asks the user to enter a new value 
        flag3 = false; // sets to false so itll go through loop again 

      }
    }
    
    Scanner oddInt = new Scanner(System.in); // inputs new scanner 
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: "); // asks user to input a value 
    boolean flag4 = false; // intializes a varible as a boolean 
    int oddDiamonds = 0; // initializes a variable 
    
    while (!flag4){ //make a loop statement for if they do not enter a positive integer
      // if the value is na integer 
      if (myScanner.hasNextInt()){
        // accepts the value as an inter 
        oddDiamonds = myScanner.nextInt();
        flag4 = true; // sets it to true 
        // if value is positive and odd 
        if (oddDiamonds > 0 && oddDiamonds % 2 != 0 ){
             flag4 = true; // sets it to true to get out of the loop
        }

        // if not positive or not odd
        else {
             System.out.print ("Error: please type in an odd integer "); // asks user to input a new value 
             flag4 = false; // sets it to false to check the value again 
        }
      }
      // if the value is not a integer 
      else {
        myScanner.next(); // discards the value 
        System.out.print("Error: please type in an integer "); // asks the user to input a new value 
        flag4 = false; // set it to false to check to go through the while loop again 

      }
    }
    
    Scanner firstCharacter = new Scanner(System.in); // declares new scanner 
    System.out.print("Please enter a first characters for the pattern fill: "); // asks user input a character 
    String first = myScanner.next(); // accepts value as a string 
    char firstChar = first.charAt(0); // gets the character at integer 0 in the string 
 
    
    Scanner secondCharacter = new Scanner(System.in); // declares new scanner 
    System.out.print("Please enter a second character for the pattern fill: "); // asks user to input a character 
    String second = myScanner.next(); // accepts value as a string
    char secondChar = second.charAt(0); // gets the character at integer 0 in the string 
   
    Scanner thirdCharacter = new Scanner(System.in); // declares new scanner 
    System.out.print("Please enter a third character for the pattern fill: "); // asks user to input a character 
    String third = myScanner.next(); // accepts value as a string 
    char thirdChar = third.charAt(0); // gets the character at integer 0 in the string 
    
    //printing the argyle 
    for (int i = 0; i <= oddDiamonds; i++){// when i is less than the width of the odd diamond it will add on ever time it goes through 
      for (int j = 0; j <= oddDiamonds - i; j++){ // when j is less then the oddDiamond value entered minus the value of i, then it adds one to j 
        System.out.print(firstChar);  // prints the first char to the surrounding around the diamond 
      }
      for(int j = 0; j <= i*2-1; j++) {
       System.out.print(secondChar); // print the triangle using the second char 
      }
      for(int j = 0; j <= oddDiamonds; j++){
       System.out.print(firstChar); // prints the first char on the other side of the  triangle 
      }
     System.out.println(); // goes to next line 
    }
    
    for (int i = oddDiamonds - 1; i > 0; i--){ // print an upside down triangle to be added below to the other triangle to create a diamond 
      for (int j = 0; j <= oddDiamonds - i; j++){
       System.out.print(firstChar); // prints the first char to the surrounding around the diamond 
     }
     for (int j = 0; j <= i*2-1; j++){
       System.out.print(secondChar); // print the upside down triangle using the second char 
     }
     
     for(int j = 0; j <= oddDiamonds -1 ; j++){
       System.out.print(firstChar); // prints the first char on the other side of the upside down triangle 
      }
      
     System.out.println(); // goes to next line 
     }
    
    /// I did the best i could. and i went to two different tutors for help. I started this the friday it was given to us and
    //I still couldn't figure it out. I spent numerous hours on the code, and still wasn't able to get it 


    }
  }
  




