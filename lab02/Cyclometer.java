// Lab02 Cyclometer
// Georgia Burlingame
// 02.02.2018 
// CSE 2


//Purpose is to print out the time and rotation of front wheel during a given time

public class Cyclometer{
  //initializes a string and variables for the calculations that will then be printed out
  
  public static void main(String[] args){
    
    //initalize variables 
    int secsTrip1 = 480; // time = 480 seconds for trip 1 
    int secsTrip2 = 3220; // time = 3220 seconds for trip 2
    int countsTrip1 = 1561; // counts = 1561 for trip 1
    int countsTrip2 = 9037; // counts = 9037 for trip 2 
    
    double wheelDiameter = 27.0; //stores the diameter of wheel in the variable as a double
    float PI = (float) 3.14159; // stores PI which will be used to calculated area of the wheel 
    double feetPerMile = 5280; //stores # of feet that can be traveled in one mile
    double inchesPerFoot = 12; //stores # of inches per foot 
    double secondsPerMinute = 60; // stores # of sec in a min
    double distanceTrip1;
    double distanceTrip2;
    double totalDistance; //creates each of these variables to be stored as a double 
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had "+ countsTrip1 + " counts."); //for trip 1 prints out # stored in variables for minutes and counts 
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); // for trip 2 prints out # stored in varianles for minutes and counts
    
    //calculations 
    
    distanceTrip1 = countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; 
    //calculates distance for trip 1 in inches
    // multiplies number of counts for trip 1 by the wheel diameter in inches then times pi 
    //calulates distance in miles by dividing by inches per foot and then divding by feet per mile 
   
    distanceTrip2  = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //does same calculation above for trip 2 
    totalDistance = distanceTrip1 + distanceTrip2; //takes sum of commputed values above to get total distance traveled
    
   //Print out data/calculations 
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
    
    
   
    
  }}

  