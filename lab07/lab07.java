// Georgia Burlingame 
//CSE 002 
// Lab 07

import java.util.Random; 
import java.util.Scanner; 

public class lab07 {
  public static void main(String[] args){
    //Random randomGenerator = new Random();
   // int randomInt = randomGenerator.nextInt(10);
    
    Scanner myScanner = new Scanner(System.in);
    String  answer1 = adjectives();
    String  answer2 = adjectives();
    String  answer3 = subject();
    String  answer4 = pastTermVerbs();
    String  answer5 = adjectives();
    String  answer6 = object();
    
    System.out.println("The " + answer1  + answer2 + answer3 + " " + answer4 + " the " + answer5 + answer6 );
    for( ; ; ){
    
    System.out.print("Would you like another sentence? yes or no ");
      String userInput = myScanner.next(); 
      if(userInput.equals("yes")){
        answer1 = adjectives();
          answer2 = adjectives();
          answer3 = subject();
          answer4 = pastTermVerbs();
          answer5 = adjectives();
          answer6 = object();
        System.out.println("The " + answer1  + answer2 + answer3 + " " + answer4 + " the " + answer5 + answer6 );
        continue;
          
      }
      
      else{
        break;
      }
    
    }
    
  }
  
  public static String adjectives (){
    Random randomGenerator = new Random();
    int i = randomGenerator.nextInt(10);
    
   // int adjectives = (int)(Math.random()*10);
   
    switch(i){
      case 1: return "small "; 
      case 2: return "big "; 
      case 3: return "fat ";  
      case 4: return "white "; 
      case 5: return "lazy "; 
      case 6: return "grumpy "; 
      case 7: return "creepy "; 
      case 8: return "pretty "; 
      case 9: return "rude "; 
      case 10: return "break "; 
    }
    return ""; 
  }
  
  public static String subject(){
    Random randomGenerator = new Random();
    int j = randomGenerator.nextInt(10);
    
  //  int subject = (int)(Math.random()*10);
    
    switch(j){
      case 1: return "dog"; 
      case 2: return "person"; 
      case 3: return "elephant";  
      case 4: return "princess"; 
      case 5: return "girl";  
      case 6: return "boy";  
      case 7: return "sloth";
      case 8: return "alien";  
      case 9: return "farmer"; 
      case 10: return "teacher"; 
    }
   return "";
  }
  
  public static String pastTermVerbs (){
    Random randomGenerator = new Random();
    int k = randomGenerator.nextInt(10);
    
   // int verb = (int)(Math.random()*10);
    
    switch(k){
      case 1: return "passed"; 
      case 2: return "ran"; 
      case 3: return "laughed"; 
      case 4: return "jumped"; 
      case 5: return "skied";
      case 6: return "sang"; 
      case 7: return "killed";  
      case 8: return "loved";  
      case 9: return "helped"; 
      case 10: return "sprinted"; 
    }
    return "";
    
  }
  
  public static String object(){
    Random randomGenerator = new Random();
    int l = randomGenerator.nextInt(10);
    
    //int objectPart = (int)(Math.random()*10);
    
    switch(l){
      case 1: return "pirate"; 
      case 2: return "prince";  
      case 3: return "troll"; 
      case 4: return "ninja"; 
      case 5: return "panda"; 
      case 6: return "deer";
      case 7: return "camper"; 
      case 8: return "celebrity";  
      case 9: return "actor"; 
      case 10: return "doctor"; 
    }
    return "";
    
  }
  
}
