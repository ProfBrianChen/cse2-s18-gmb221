// HW02 
// Georgia Burlingame 
// CSE 2
// 02.06.2018

public class Arithmetic{ 
  public static void main(String[] args){
  
  // initialize variables 
  int numPants = 3; // # of pants
  double pantsPrice = 34.98; //cost of pair of pants
  
  int numShirts = 2; // # of shirts
  double shirtPrice = 24.99; //price of a shirt
  
  int numBelts = 1; // Number of belts
  double beltCost = 33.99; // cost of a belt
  
  double paSalesTax = 0.06; // PA tax rate
  
  double totalPantsCost; //total cost of pants
  double totalShirtsCost; //total cost of shirts
  double totalBeltsCost; //total cost of belts 
  double taxPants; //tax for pants
  double taxShirts; //tax for shirts 
  double taxBelts; //tax for belts 
  double totalPurchaseCost; //cost of all the items
  double totalSalesTax; //cost of sales tax for all items 
  double totalPaid; //cost of purchase including sales tax 
  double taxOfPantsRounded; // creates variable for rounded tax value of pants
  double taxOfShirtsRounded; // creates variable for rounded tax value of shirts
  double taxOfBeltsRounded; // creates variable for rounded tax value of belts
  double totalPantsCostRounded; // creates variable for rounded pants cost
  double totalShirtsCostRounded; // creates variable for rounded shirts cost
  double totalBeltsCostRounded;  // creates variable for rounded belts cost
  double totalPaidRounded; // creates variable for rounded total purchase price w/ sales tax 
   
    
  //calculations
  totalPantsCost = numPants*pantsPrice*100; // total cost of pants by multiplying # and cost
  totalShirtsCost = numShirts*shirtPrice*100; // total cost of shirts by multiplying # and cost
  totalBeltsCost = numBelts*beltCost*100; // total cost of belts by multiplying # and cost
  
  //converts  each variable that is calculated above into a new variable, making it an integer
  int rtotalPantsCost = (int) totalPantsCost; 
  int rtotalShirtsCost = (int) totalShirtsCost;
  int rtotalBeltsCost = (int) totalBeltsCost;
  
  // divides the integer variables by 100 to make values with 2 decimal places 
  totalPantsCostRounded = rtotalPantsCost/100.0;
  totalShirtsCostRounded = rtotalShirtsCost/100.0;
  totalBeltsCostRounded = rtotalBeltsCost/100.0;
  
  System.out.println("Total cost of pants is $" + totalPantsCostRounded); // prints cost of all pants
  System.out.println("Total cost of shirts is $" + totalShirtsCostRounded); // prints cost of all shirts
  System.out.println("Total cost of belts is $" + totalBeltsCostRounded); // prints cost of all belts
  
  taxPants = totalPantsCostRounded*paSalesTax*100; // sales tax calculation by multiplying total price by sales tax
  taxShirts = totalShirtsCostRounded*paSalesTax*100; // sales tax calculation by multiplying total price by sales tax
  taxBelts = totalBeltsCostRounded*paSalesTax*100; // sales tax calculation by multiplying total price by sales tax 
  
  int rTaxPants = (int) taxPants; // makes it into an integer 
  int rTaxShirts = (int) taxShirts; // makes it into an integer
  int rTaxBelts = (int) taxBelts; // makes it into an integer 
  
   //makes tax rounded to 2 decimal places
  taxOfPantsRounded = rTaxPants/100.0; 
  taxOfShirtsRounded = rTaxShirts/100.0; 
  taxOfBeltsRounded = rTaxBelts/100.0;
    
  
  System.out.println("Total sales tax for buying all of the pants is $" + taxOfPantsRounded); // prints cost of sales tax of pants
  System.out.println("Total sales tax for buying all of the shirts is $" + taxOfShirtsRounded); // prints cost of sales tax of shirts
  System.out.println("Total sales tax for buying all of the belts is $" + taxOfBeltsRounded); // prints cost of sales tax of belts
  
  totalPurchaseCost = totalPantsCostRounded + totalShirtsCostRounded + totalBeltsCostRounded; // calculates total purchases without tax by adding cost of each item

  totalSalesTax = taxOfPantsRounded + taxOfShirtsRounded + taxOfBeltsRounded; // calculates total sales tax by adding sales tax of each item 
  totalPaid = (totalPurchaseCost + totalSalesTax)*100; // calculates total amount by adding total sales tax and total purchase cost
  
  int rtotalPaid = (int) totalPaid; // makes it into an integer 
  
  totalPaidRounded = rtotalPaid/100.0; // converts total cost so that it is rounded ot two decimal places
    
  System.out.println("The total cost of purchases before tax is $" + totalPurchaseCost); // prints total purchase price of all items without tax
  System.out.println("The total sales tax for all of the items purchased is $" + totalSalesTax); //prints total sales tax 
  System.out.println("The total paid including sales tax is $" + totalPaidRounded); // prints total paid for items including sales tax 
  
}}