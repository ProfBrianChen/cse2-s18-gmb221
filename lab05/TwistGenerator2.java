import java.util.Scanner;

 

public class TwistGenerator2 { 
  public static void main(String[] args){
    Scanner myScanner = new Scanner ( System.in ); //create a new scanner
    System.out.print ("Enter a positive interger: "); //asks the user to input a positive integer
    int length = 0; //this is how they will input integer
    boolean flag = false;

    while (!flag){ //make a loop statement for if they do not enter a positive integer
      if (myScanner.hasNextInt()){
        length = myScanner.nextInt();
        flag = true;
        if (length > 0){
             flag = true;
        }

        else {
             System.out.print ("Error: please type in a positive integer ");
             flag = false;
        }
      }

      else 
        myScanner.next();
        System.out.print("Error: please type in an integer ");
        flag = false;

      }
    }

    for (int i = 0; i < length ; i++){
      if ( i % 3 == 0){
        System.out.print ("\\");
      }

      else if (i % 3 ==1){
        System.out.print (" ");
      }

      else if (i % 3 ==2){
        System.out.print ("/");
      }
    }

     System.out.println();

    for (int i = 0; i < length; i++){
      if ( i % 3 == 1){
        System.out.print ("X");
      }

      else {
        System.out.print (" ");
      }
    }

    System.out.println();

    for (int i = 0; i < length ; i++){
      if ( i % 3 == 0){
        System.out.print ("/");
      }

      else if (i % 3 ==1){
       System.out.print (" ");
      }

      else if (i % 3 ==2){
        System.out.print ("\\");
      }

    }

     System.out.println();

  }
}
