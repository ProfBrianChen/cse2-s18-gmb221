/* Georgia Burlingame 
CSE 2
HW 09
Poker */


import java.util.Random; // import random
import java.util.Arrays; // imports arrays 

public class DrawPoker{
  public static void main(String[] args){
    int[] deck = new int[52]; // shuffled deck array 
    
    for(int i = 0; i<deck.length; i++){
      deck[i] = i;
    }
    
  
    
    int[] shuffled = shuffle(deck);
 
    
    int[] player1 = new int[5]; // establishes a new array for the cards that will be delt to each player 
    int[] player2 = new int[5]; // establishes a new array for the cards that will be delt to each playe
    

    player1[0] = shuffled[0]; //first card delt to player one 
    player2[0] = shuffled[1]; // second card to player two
    
    player1[1] = shuffled[2]; // third card to player one 
    player2[1] = shuffled[3];  // fourth card to player two 
   
    player1[2] = shuffled[4]; 
    player2[2] = shuffled[5];   
   
    player1[3] = shuffled[6];
    player2[3] = shuffled[7];

    player1[4] = shuffled[8];
    player2[4] = shuffled[9];

    printDeck(player1); //
    printDeck(player2);
    
    //see what the highest hand player one has
    int playerOneScore = 0; // intializes counter 
    int playerTwoScore = 0; 
    
    if(fullHouse(player1)){
      playerOneScore  = 5; //get most points for getting a full house and it lowers from there as noticeable 
    }
    else if(flush(player1)){
      playerOneScore = 4;
    }
    else if(threes(player1)){
      playerOneScore = 3;
    }
    else if(pair(player1)){
      playerOneScore = 2;
    }
   
    //highest hand for player 2
    if(fullHouse(player2)){
          playerTwoScore  = 5;
    }
    else if(flush(player2)){
          playerTwoScore  = 4;
    }
    else if(threes(player2)){
         playerTwoScore  = 3;
    }
    else if(pair(player2)){
         playerTwoScore  = 2;
    }
   
    System.out.println("player one: " + playerOneScore);
    System.out.println("player two: " + playerTwoScore);
    // if the player one score is greater than player one then player one wins and it will then check how player one won

    if(playerOneScore > playerTwoScore ){
      System.out.print("Player One wins. ");
     
      if(fullHouse(player1)){
      System.out.println("Player One had a full house ");
    }
      else if(flush(player1)){
       System.out.println("Player One had a flush ");
    }
      else if(threes(player1)){
         System.out.println("Player One had a three of a kind ");
    }
      else if(pair(player1)){
         System.out.println("Player One had a pair ");
    }
      else{
         System.out.println("Player One had the high card hand ");
    }


    }
  
    // if the player two score is greater than player one then player two wins and it will then check how player two won
  else if(playerOneScore <  playerTwoScore ){
      System.out.print("Player Two wins. ");
    
    if(fullHouse(player2)){
      System.out.println("Player Two had a full house ");
    }
    else if(flush(player2)){
       System.out.println("Player Two had a flush ");
    }
    else if(threes(player2)){
         System.out.println("Player Two had a three of a kind ");
    }
    else if(pair(player2)){
         System.out.println("Player Two had a pair ");
    }
    else{
         System.out.println("Player Two had the high card hand ");
    }
    }
  
  }
 
  
  
  
  public static void printDeck(int[] cardsHand){
    for(int i = 0; i < cardsHand.length; i++){
   
      System.out.print(cardsHand[i] + " ");
    }
    System.out.println();
}
   

  //shuffles the deck of cards
  public static int[] shuffle(int[] cards){
    Random randomGenerator = new Random();
    for (int i = 0; i < cards.length; i++){
      int randomNumber = (int)(Math.random()*cards.length);// random number 
      int temp = cards[randomNumber];// stores number at i index in array temporarily
      cards[randomNumber] = cards [i];
      cards[i] = temp;
   
    }
    return cards; //returns the deck of shuffled cards 
  }
  
  public static boolean pair(int[] hand){
    int count0 = 0; //initializes a counter for each number of card
    int count1 = 0; 
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count6 = 0;
    int count7 = 0;
    int count8 = 0;
    int count9 = 0;
    int count10 = 0;
    int count11 = 0;
    int count12 = 0;
    
  
    for(int i = 0; i < hand.length; i++){ //loops through the players hand to find what number cards they have
     int card = hand[i];
      int card2 = card%13;
      switch(card2){
        case 0:
          count0++;
          break;// counter for the number of 
        case 1:
          count1++; 
          break;//counter for the amount of ones and so on continuing down
          
        case 2:
          count2++;
          break;
          
        case 3:
          count3++;
          break;
       
        case 4:
          count4++;
          break;
          
        case 5:
          count5++;
          break;
          
        case 6:
          count6++;
          break;
          
        case 7:
          count7++;
          break;
          
        case 8:
          count8++;
          break;
          
        case 9:
          count9++;
          break;
       
        case 10: 
          count10++;
          break;
        
        case 11:
          count11++;
          break;
        
        case 12:
          count12++;
          break;
      }
    }
    // if the count is graters than or equal to 2, then they have a pair
    if(count0 ==2 || count1 == 2 || count2 == 2 || count3 == 2 || count4 == 2 || count5 == 2 || count6 == 2 ||
      count7 == 2 || count8 == 2 || count9 == 2 || count10 == 2 || count11 == 2 || count12 == 2){
  

      return true; // returns true 
    }
    else{
                     

      return false; // if not greater than or equal to to then it is false
    }
  }
  
  public static boolean threes(int[] hand){
    int count0 = 0; //intializes counters 
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count6 = 0;
    int count7 = 0;
    int count8 = 0;
    int count9 = 0;
    int count10 = 0;
    int count11 = 0;
    int count12 = 0;
    
    for(int i = 0; i < hand.length; i++){
      switch(hand[i]%13){ // evaluates the index mod 13 to see if it is 0, 1, 2, 3, etc. //which counts how many of each card there is 
        case 0:
          count0++; 
          break;// counts the number of non face cards in the hand 
        case 1:
          count1++; 
          break;// counts the number of non face cards in the hand 
        case 2:
          count2++;
          break;// counts the number of non face cards in the hand 
        case 3:
          count3++;
          break;// counts the number of non face cards in the hand 
        case 4:
          count4++;
          break;// counts the number of non face cards in the hand 
        case 5:
          count5++;
          break;// counts the number of non face cards in the hand 
        case 6:
          count6++;
          break;// counts the number of non face cards in the hand 
        case 7:
          count7++;
          break;// counts the number of non face cards in the hand 
        case 8:
          count8++;
          break;// counts the number of non face cards in the hand 
        case 9:
          count9++;
          break;// counts the number of non face cards in the hand 
        case 10: 
          count10++;
          break;// counts the number of non face cards in the hand 
        case 11:
          count11++;
          break;// counts the number of non face cards in the hand 
        case 12:
          count12++;
          break;// counts the number of non face cards in the hand 
      }
    }
    if(count0 == 3 || count1 == 3|| count2 == 3 || count3 == 3 || 
        count4 == 3 || count5 == 3 || count6 == 3 ||count7 == 3 ||
       count8 == 3 || count9 == 3 || count10 == 3 || count11 == 3 ||
        count12 == 3){ // there are 3s of a kind 
      return true; // if a three of a kind exists then returns true 
    }
    else{
      return false; // if not returns false
    }
  }




  public static boolean flush(int[] hand){
    int diamonds = 0;// declare a counter for each suit
    int clubs = 0;
    int hearts = 0;
    int spades = 0;
    
    for(int i = 0; i < hand.length; i++){
      if ((int)hand[i] >= 0 && (int)hand[i] <= 12){
        diamonds ++; // counts the number of face cards in the hand 
      }
      if ((int)hand[i]  > 13 && (int)hand[i] <= 25){
        clubs ++; // counts the number of face cards in the hand 
      }
      if ((int)hand[i]  > 26 && (int)hand[i] <= 38){
        hearts ++; // counts the number of face cards in the hand 
      }
      if ((int)hand[i] > 39 && (int)hand[i] <= 51){
        spades ++; // counts the number of face cards in the hand 
      }
      
    }
    
    //if the counter is equal to the number of cards in the hand then the player has a flush then return true
    if((diamonds == hand.length || clubs == hand.length) || (hearts == hand.length || spades == hand.length)){
      return true; // if they have a flush returns true 
    }
    else{
      return false; // if they do not have a flush 
    }
    
  }

  public static boolean fullHouse(int[] hand){ //if they have a pair and a double and return true then they have a full house therefore return ture 
    //printDeck(hand);
    if(pair(hand) && threes(hand)){ // checks to see if equal 
      return true; // returns true if they are 
    }
    else{

      return false; // returns falls if not
    }
  }
  
}

  
  
  