// Georgia Burlingame 
//CSE 002 
// HW 07, Program 1


import java.util.Scanner; //import scanner  

public class Area{
  public static void main(String[] args){
    Scanner shape = new Scanner(System.in);
    System.out.print("Enter the shape that you want to find the area of (triangle, rectangle or circle): "); // ask user to enter a string of the shape they want to find the area of 
    String shapeForArea = ""; // inputs a string 
    boolean flag = false; // intializes a flag to false so that it will go through loo
    
    while (!flag){ // while false is true 
      
      if(shape.hasNextLine()){ // if what was entered by the user is a string 
        shapeForArea = shape.nextLine();  // accepts it as a string 
        flag = true; // changes flag to true so that it will stop going through while loop 
        if (shapeForArea.equals("triangle") || shapeForArea.equals("rectangle") || shapeForArea.equals("circle")){ // if the string they entered is one of these shapes
           flag = true; // sets the flag to true so that it will break out of loop 
        }
        else{ // if a string, but not one of the strings that we want 
          System.out.print("Error. Please type in triangle, rectangle, or circle as the shape "); // asks them to print a string that we want 
          flag = false; // sets flag to false so that it will go through while loop again 
        }
      }
      
      else { // if it is not a string
        shape.next(); // discards value 
        System.out.print("ERROR! Enter a string type. "); //asks them to enter a new string
        flag = false; // sets flag to false so it will go through the loop again 
      }

    }
  

   if (shapeForArea.equals("triangle")){ // if the string the user entered was a triangle 
     double height = input("Please enter the height: ", shape); // calls input method for the height to see if the value entered was a double, won't accept the value until it is double 
     double base = input("Please enter the base: ", shape); // calls input method for the width to see if the value entered was a double 
     double triangleArea = triangle(height, base); // uses the doubles you entered and calls the method for area and calculates the area 
     System.out.println("The area of the triangle is " + triangleArea); // prints the area 
     
   }
    
   if (shapeForArea.equals("rectangle")){ // if the string the user entered was rectangle 
     double length = input("Please enter the length: ", shape); // calls input method for the length to see if the value entered was a double 
     double width =  input("Please enter the width: ", shape); // calls input method for the width to see if the value entered was a double 
     double rectangleArea = rectangle(length, width); // using the doubles that you entered and already checked for length and width this command calls the area method to calculate the area 
     System.out.println("The area of the rectangle is " + rectangleArea); // prints the area of the rectangle 
     
      
   }
    
   if (shapeForArea.equals("circle")){ // if the string the user entered was circle 
     double radius = input("Please enter the radius: ", shape); // calls input method for the radius to see if the value entered was a double 
     double circleArea = circle(radius); // uses doubles that you entered already and calls the method for area of circle and calculates area 
     System.out.println("The area of the circle is " + circleArea ); // prints area 
    }
    
  }
  
  
  
  
  
  public static double triangle(double height, double base){
    return (.5)*height*base; // calulates area and returns it back to triangleArea in main method 
    
  }
  
  
  
  
  
  public static double rectangle(double length, double width){
    return length*width; // calculates area and returns it back to rectangleArea in main method
  }
  
  
  
  
  
  public static double circle(double radius){
    double areaCircle; // initializes variable 
    areaCircle = Math.PI * (radius * radius); // formula for calculating the area 
    return areaCircle; // calculates area and returns it back to the circleArea in main method 
    
  }
  
  
  
  
  
  
  public static double input(String prompt, Scanner myScan){
    boolean flag1 = false; // flag boolean so that it will run through the loop 
    System.out.print(prompt); // prints out the prompt that was entered in the main method
   while(!flag1){ // while true 
    
     if (myScan.hasNextDouble()){ // if the value entered is a double 
       double finalNum = myScan.nextDouble(); // accepts it as a double in the variable finalNum
       return finalNum; // returns the finalNum to the variable that was calculated 
     }
     
     else{ // if not a double 
       myScan.next(); //discards value 
       System.out.println("Error. Please enter a double: "); // asks user to enter in a double 
     }
        
   }
   
      return 0;   // return zero so method is finished 
      
  } 
}