// Georgia Burlingame
// CSE 002
//HW 07, Program 2

import java.util.Scanner; //imports scanner 

public class StringAnalysis{
  public static void main(String[] args){
    
    // if they want to examine all of the characters in the string call the method inside the if statement 
    Scanner myScan = new Scanner(System.in); // creates scanner 
    String choice1 = "yes"; // string for answer
    String choice2 = "no"; // string for answer 
    boolean flag; // boolean variable 
    
    System.out.print("Enter a string of characters: ");
    String letters = myScan.next();
    
    System.out.print("Do you examine all of the characters in the string? yes or no ");
    String choice = myScan.next();
    
    while (!choice.equals(choice1) && !choice.equals(choice2)){
      System.out.print("ERROR! Please type in yes or no: ");
      choice = myScan.next();
    }
    
    if(choice.equals(choice1)){
      flag = Check(letters);
      if(flag == true){
        System.out.println("All the characters you entered are letters ");
      }
      else{
        System.out.println("Some of the characters you entered are not letters ");
        
      }
    }
    
    else if (choice.equals(choice2)){
      int numOfCharacters = input("Enter the number of characters you want to check: ", myScan);
      flag = Check(letters, numOfCharacters);
      if(flag == true){
        System.out.println("All of the characters you entered are letters ");
        
      }
      else {
        System.out.println("Some of the characters you entered are not letters");
      }
    }
  }
    
        
  public static boolean Check(String word){
    boolean flag; // declares boolean 
    int i = 0; // declares variable 
    int lengthOfWord = word.length(); // gets the length of word inputted 
    do{
      char current = word.charAt(i++);
      if (('a' <= current) && ('z' >= current)){ // if all are character
        flag = true; // sets variable to true that will be returned to end while statement 
      }
      else{
        flag = false; // not all are characters to it sets it to false to go through again 
      }
    } while ((flag == true) && (i<lengthOfWord)); // continues while flag is true and until you get to the end of the word
    return flag; //returns variable back to main 
  }

  
  
  
  public static boolean Check (String word, int j){
    boolean flag; // declares variable 
    int i = 0; // declares variable 
    int length = word.length(); // length of word inputted 
    
    if ( j < length){ // if number of character you want to evaluate is less then the length of the word
      j = j;  // evaluate that number of characters   
    }
    
    else{
      j = length; // if the number entered is greater than the length then it will evaluate the whole length of word
    }
    
    do{
      char current = word.charAt(i++); // adds to i to examine the next character 
      if (('a' <= current) && ('z' >= current)){ // if all are character
        flag = true; // sets variable to true that will be returned to end while statement 
      }
      else{
        flag = false; // not all are characters to it sets it to false to go through again 
      }
    } while ((flag == true) && (i<j)); // continues while flag is true and until you get to the end of the amount of characters you want to evaluate 
    return flag; //returns variable back to main 
  }
  
  
  
  
  
  public static int input(String prompt, Scanner myScan){
    boolean flag = false; // initializes boolean
    System.out.print(prompt); // prints prompt from main method 
    while(!flag){ // while true 
    
     if (myScan.hasNextInt()){ // check if it is an int
       int finalNum = myScan.nextInt(); // if it is accept the number 
       return finalNum; // return that number 
     }
     else{
       myScan.next(); // else discard the number 
       System.out.println("Error. Please enter an integer: "); // print a statement to ask again; 
     }
        
   }
   
      return 0;   // end the method 
      
  }
  
}
 
