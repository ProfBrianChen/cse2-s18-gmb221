/* Georgia Burlingame 
hw10 
robot city hw 
CSE 2 
Prof Chen */

import java.util.Arrays;
import java.util.Random; 

public class RobotCity{
  public static void main(String[] args){
    Random ranGen = new Random();
    int robotsInvading = (int)((Math.random()*100) + 1); // random number of invading robots 
    
    
    int[][] array1 = buildCity(); //builds city and sets it equal to 
    display(array1); //displays the city block 
    array1 = invade(array1, robotsInvading); //has robots invade 
    display(array1); //displays what it looks like with invading robots 
    
    for(int i = 0; i < 5; i++){
      array1 = update(array1); // updates array
      display(array1); // displays array
    }
    
  }
  
  // method that builds city 
  public static int[][] buildCity(){
    Random randomGenerator = new Random();
    int northSouth = (int)((Math.random()*5)+10); // values between 10-15
    int eastWest = (int)((Math.random()*5)+10);
    
    int [][] cityBlockArray = new int[northSouth][eastWest]; // creates northSouth part of the block using an array
   
    for(int k = 0; k < cityBlockArray.length; k++){
      for(int j = 0; j < cityBlockArray[k].length; j++){
        int pop = (int)((Math.random() * 900) + 100); //values betweeen 100-999
        cityBlockArray[k][j] = pop; // the value of the pop goes to the the value of the index j
      }
    }
    return cityBlockArray; //returns array to method
  }
    
  
  
  public static void display(int[][] cityArray){
    for(int k = 0; k < cityArray.length; k++){
      for(int j = 0; j < cityArray[k].length; j++){
        System.out.printf(cityArray[k][j] + " "); // prints two dimensional array 
      }
      System.out.printf("\n"); // prints new line 
    }
    System.out.println(); //prints line to separate the updates
  }
  
  
  public static int[][] invade(int[][] cityArray, int k){
    Random ranGenerator = new Random();
    boolean flag = true; // flag boolean for the while loop 
    // when the invader lands on a population, the population becomes negative and that makes that the invader is there 
    for(int i = 0; i <= k; i++){
      flag = true; // sets flag to true so that it will enter loop every time you have a new number 
      while (flag){
        int northSouth = new Random().nextInt(cityArray.length); // generates new coordinates bc was already occupied 
        int eastWest = new Random().nextInt(cityArray[northSouth].length); // generates new coordinates 
        

        if(cityArray[northSouth][eastWest] > 0){ // if the coordinate is positive then an invador will land on it 
          cityArray[northSouth][eastWest] *= -1; // sets the value to a negative to say that an invader is now occupying this spaces
          flag = false; //sets value to false so that it breaks out of the while loop 
        }
  
        else if(cityArray[northSouth][eastWest] < 0){ // if the coordinate is negative then it will go back through the while loop and get new coordinates 
          flag = true; // breaks out of while statement and goes back to for loop by adding one 
        }
      }
    }
    return cityArray; //returns array to print it out 
  }
  
  
  public static int [][] update(int[][] cityArray){
    for(int i = 0; i <= cityArray.length - 1; i++){
      for(int j = 0; j <= cityArray[i].length - 1; j++){
        if ((cityArray[i][j] < 0) && ((j+1) < cityArray[i].length - 1) && (j != cityArray[i].length-1)){ // if the value at index is less than one and if the j+1 is less than the length of the array minus 1 and if j isnt equal to the last column of the array 
          cityArray[i][j+1] *= -1; //sets the value at the nindex nect to it as negative 
        }
       
        else if ((j == cityArray[i].length-1) && cityArray[i][j] < 0){
          cityArray[i][j] *= -1; // will change the last column values to positive if they are negative 
        }
        
      }
    }
    return cityArray; //returns the array 
  }

}
