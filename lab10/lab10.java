/* Georgia Burlingame 
CSE2 
Lab 10 Matrix
*/

import java.util.Arrays;
import java.util.Random;

public class lab10{
  public static void main(String[] args){
    Random randomGen = new Random();
    
    int width = (int)(Math.random()*6);
    int height = (int)(Math.random()*6);
    System.out.println(width);
    System.out.println(height);
    int width2 = (int)(Math.random()*6);
    int height2 = (int)(Math.random()*6);
    
    int[][] matrixA = increasingMatrix(width, height, true);
    printMatrix(matrixA, true);
  //  int[][] matrixB = increasingMatrix(width, height, false);
  //  printMatrix(matrixB, false);
  }
  
  public static int[][] increasingMatrix(int width, int height, boolean format){
    
    if (format == true){
      int[][] matrix = new int[height][width]; // creates new array 
      int valueAtIndex = 0;
      for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
          matrix[i][j] = valueAtIndex;
          valueAtIndex ++;
        }
      }
      return matrix;
    }
     
    else{
      int[][] matrix = new int[height][width]; // creates new array
      int valueAtIndex = 0; 
      for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++){
          matrix[i][j] = valueAtIndex;
          valueAtIndex ++; 
        }
      }
      return matrix;
    }
  }

  
  public static void printMatrix(int[][] array, boolean format){
    if(array == null){
      System.out.println("the array was empty");
    }
    else if(format == true){
      for(int i = 0; i < array.length; i++){
        System.out.print("[");
        for(int j = 0; j < array[i].length; j++){
          System.out.printf(array[i][j] + " "); // prints two dimensional array 
        }
        System.out.print("]");
        System.out.printf("\n"); // prints new line 
      }
    }
    
    
  }

 // public static 
  
  
}